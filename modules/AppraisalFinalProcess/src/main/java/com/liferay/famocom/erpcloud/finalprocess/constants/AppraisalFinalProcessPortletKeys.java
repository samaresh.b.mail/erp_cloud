package com.liferay.famocom.erpcloud.finalprocess.constants;

/**
 * @author Samaresh Barui
 */
public class AppraisalFinalProcessPortletKeys {

	public static final String AppraisalFinalProcess = "AppraisalFinalProcess";
	public static final String Earning = "E";
	public static final String Deduction = "D";
	public static final String Reimbursement = "R" ;
	public static final String Overtime ="O";
	public static final String StatutoryComponent ="S";
	public static final String EARNING = "EARNING";
	public static final String DEDUCTION = "DEDUCTION";
	public static final String REIMBURSEMENT = "REIMBURSEMENT" ;
	public static final String OVERTIME ="OVERTIME";
	public static final String STATUTORYCOMPONENT ="STATUTORY COMPONENT";
	public static final String Taxable ="TAXABLE";
	public static final String nonTaxable ="NONTAXABLE";
	public static final long AppraisalSettingActive = 1;
	public static final long AppraisalFinalSubmmit = 3;
	public static final long AppraisalSettingLocked = 1;
	public static final long SubmitStepForReviewed = 3;
}